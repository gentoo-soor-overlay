# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

inherit autotools flag-o-matic

RESTRICT="primaryuri"

DESCRIPTION="Open source web browser engine"
HOMEPAGE="http://www.webkit.org/"
MY_P="WebKit-r${PV}"
SRC_URI="http://nightly.webkit.org/files/trunk/src/${MY_P}.tar.bz2"

LICENSE="LGPL-2 LGPL-2.1 BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~ppc"
IUSE="debug gstreamer hildon +pango soup +sqlite +svg"

S="${WORKDIR}"/${MY_P}

# FIXME: ask 'alp' or 'kalikiana' about libsoup and pango
# version needed.
RDEPEND="x11-libs/gtk+:2
		 dev-libs/icu
		 media-libs/jpeg
		 media-libs/libpng:1.2
		 dev-libs/libxslt
		 dev-libs/libxml2:2
		 !pango? ( media-libs/freetype:2 )
		 pango? ( >=x11-libs/pango-1.20 )
		 !soup? ( >=net-misc/curl-7.15 )
		 soup? ( net-libs/libsoup:2.4 )
		 sqlite? ( dev-db/sqlite:3 )
		 gstreamer? (
					  media-libs/gstreamer:0.10
					  media-libs/gst-plugins-base:0.10
					  gnome-base/gnome-vfs:2
					)"

DEPEND="${RDEPEND}
		sys-devel/bison
		dev-util/gperf
		>=sys-devel/flex-2.5.33"

pkg_setup() {
	if use pango && ! built_with_use 'x11-libs/pango' X; then
		eerror "You must build pango with USE=X in order"
		eerror "to use it as a font backend for webkit-gtk"
		die "please rebuild pango with X enabled"
	fi
	if ! use pango && ! built_with_use 'media-libs/freetype:2' X; then
		eerror "You must build pango with USE=X in order"
		eerror "to use it as a font backend for webkit-gtk"
		die "please rebuild pango with X enabled"
	fi
	if use soup && ! built_with_use 'net-libs/libsoup:2.4' ssl; then
		eerror "You must build libsoup with USE=ssl in order"
		eerror "to use it as an http backend for webkit-gtk"
		die "please rebuild libsoup with ssl enabled"
	fi
	if ! use soup && ! built_with_use --missing false -o 'net-misc/curl' gnutls nss ssl; then
		eerror "You must build curl with USE gnutls or nss or ssl in order"
		eerror "to use it as an http backend for webkit-gtk"
		die "please rebuild libsoup with one of gnutls nss or ssl enabled"
	fi
}

src_unpack() {
	unpack ${A}
	cd "${S}"

	eautoreconf
}

src_compile() {
	strip-flags

	local myopts

	if use soup; then
		einfo "Selected libsoup http backend."
		myopts="--with-font-backend=soup"
	else
		einfo "Selected curl http backend."
		myopts="--with-font-backend=curl"
	fi

	if use pango; then
		einfo "Using pango font backend."
		myopts="${myopts} --with-font-backend=pango"
	else
		einfo "Using freetype font backend."
		myopts="${myopts} --with-font-backend=freetype"
	fi

	econf ${myopts} \
		$(use_with hildon) \
		$(use_enable sqlite database) \
		$(use_enable sqlite icon-database) \
		$(use_enable sqlite dom-storage) \
		$(use_enable gstreamer video) \
		$(use_enable svg svg-experimental) \
		$(use_enable debug)

	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}
