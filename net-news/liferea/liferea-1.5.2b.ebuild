# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

WANT_AUTOMAKE=1.7
WANT_AUTOCONF=latest
inherit gnome2 eutils

DESCRIPTION="News Aggregator for RDF/RSS/CDF/Atom/Echo/etc feeds"
HOMEPAGE="http://liferea.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64 ~ppc ~ppc64 ~sparc ~x86 ~x86-fbsd"
IUSE="avahi dbus gtkhtml gnutls libnotify lua networkmanager webkit xulrunner"

RDEPEND="xulrunner? ( net-libs/xulrunner:1.8 )
		 !xulrunner? ( webkit? ( net-libs/webkitgtk ) )
		 !amd64? ( !xulrunner? ( !webkit? ( gtkhtml? ( gnome-extra/gtkhtml:2 ) ) ) )
		 >=x11-libs/gtk+-2.8:2
		 x11-libs/pango
		 gnome-base/gconf:2
		 >=dev-libs/libxml2-2.6.27:2
		 >=dev-libs/libxslt-1.1.19
		 >=dev-db/sqlite-3.3:3
		 dev-libs/glib:2
		 gnome-base/libglade:2.0
		 avahi? ( net-dns/avahi )
		 dbus? ( >=dev-libs/dbus-glib-0.71 )
		 gnutls? ( net-libs/gnutls )
		 libnotify? ( >=x11-libs/libnotify-0.3.2 )
		 lua? ( >=dev-lang/lua-5.1 )
		 networkmanager? ( net-misc/networkmanager )"

DEPEND="${RDEPEND}
		dev-util/pkgconfig
		>=dev-util/intltool-0.35"

DOCS="AUTHORS ChangeLog NEWS README"

S="${WORKDIR}"/${PN}-1.5.2

pkg_setup() {
	# Backends are now mutually exclusive.
	# we prefer xulrunner over webkit over gtkhtml
	if use xulrunner; then
		einfo "Select XULrunner 1.8 backend."
		G2CONF="--enable-xulrunner --disable-webkit --disable-gtkhtml2"
	elif use webkit; then
		einfo "Selected webkit backend."
		ewarn "WARNING: The WebKit backend is highly experimental!"
		G2CONF="--disable-xulrunner --enable-webkit --disable-gtkhtml2"
	elif use gtkhtml; then
		if ! use amd64; then
			einfo "Selected GtkHTML2 backend."
			G2CONF="--disable-xulrunner --disable-webkit --enable-gtkhtml2"
		else
			eerror "GtkHTML2 backend is not supported on amd64 anymore."
			eerror "Please choose either the xulrunner or the webkit backend."
			die "Choose either the xulrunner or the webkit backend"
		fi
	else
		ewarn "You must choose one backend for liferea to work."
		ewarn "Choices are: xulrunner, webkit, gtkhtml."
		die "You must enable one of the backends"
	fi

	G2CONF="${G2CONF} \
		--disable-gecko \
		$(use_enable avahi) \
		$(use_enable dbus) \
		$(use_enable gnutls) \
		$(use_enable libnotify) \
		$(use_enable lua) \
		$(use_enable networkmanager nm)"
}

src_install() {
	gnome2_src_install
	rm -f "${D}"/usr/bin/${PN}
	mv "${D}"/usr/bin/${PN}-bin "${D}"/usr/bin/${PN}
}
