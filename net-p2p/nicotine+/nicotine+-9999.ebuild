# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

inherit distutils eutils subversion multilib toolchain-funcs

DESCRIPTION="A fork of nicotine, a Soulseek client in Python"
HOMEPAGE="http://nicotine-plus.sourceforge.net"
SRC_URI=""
ESVN_REPO_URI="http://nicotine-plus.org/svn/trunk/nicotine+"
LICENSE="GPL-3 LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~x86-fbsd"
IUSE="geoip spell vorbis"

RDEPEND="virtual/python
		 dev-python/pygtk:2
		 vorbis? ( >=dev-python/pyvorbis-1.4-r1
				   dev-python/pyogg )
		 geoip? ( dev-python/geoip-python
				  dev-libs/geoip )
		 spell? ( dev-python/sexy-python )
		 !net-p2p/nicotine"

DEPEND="${RDEPEND}
		dev-util/pkgconfig
		sys-apps/sed"

src_unpack() {
	subversion_fetch
	cd "${S}"
}


src_install() {
	distutils_python_version
	distutils_src_install --install-lib \
		/usr/$(get_libdir)/python${PYVER}/site-packages
}
