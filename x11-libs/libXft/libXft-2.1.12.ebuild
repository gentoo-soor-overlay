# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

inherit x-modular flag-o-matic

DESCRIPTION="X.Org Xft library"

KEYWORDS="alpha amd64 arm hppa ia64 mips ppc ppc64 s390 sh sparc x86 ~x86-fbsd"

IUSE="+newspr"

RDEPEND="x11-libs/libXrender
		 x11-libs/libX11
		 x11-libs/libXext
		 x11-proto/xproto
		 newspr? (
				   >=media-libs/freetype-2.3.5-r2
				   >=media-libs/fontconfig-2.5.0-r1
				 )
		 !newspr? (
					media-libs/freetype:2
					>=media-libs/fontconfig-2.2
				  )"
DEPEND="${RDEPEND}"

pkg_setup() {
	if use newspr && \
		! built_with_use --missing false media-libs/fontconfig ubuntu; then
		eerror "You need to rebuild fontconfig with ubuntu USE enabled"
		eerror "before you can compile libXft with newspr."
		die "Please rebuild fontconfig with ubuntu enabled."
	fi

	# No such function yet
	# x-modular_pkg_setup

	# (#125465) Broken with Bdirect support
	filter-flags -Wl,-Bdirect
	filter-ldflags -Bdirect
	filter-ldflags -Wl,-Bdirect
}
src_unpack() {
	unpack ${A}
	cd "${S}"/src
	if use newspr; then
		epatch "${FILESDIR}"/${PN}-dont_interfere_with_newspr.patch.bz2 || die
	fi
}

pkg_postinst() {
	elog "DO NOT report bugs to Gentoo's bugzilla"
	elog "See http://forums.gentoo.org/viewtopic-t-511382.html for support topic on Gentoo forums."
}
