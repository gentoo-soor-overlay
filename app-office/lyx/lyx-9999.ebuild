# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

inherit autotools subversion eutils flag-o-matic font toolchain-funcs

FONT_P="latex-xft-fonts-0.1"

DESCRIPTION="WYSIWYM frontend for LaTeX, DocBook, etc."
HOMEPAGE="http://www.lyx.org/"
ESVN_REPO_URI="svn://svn.lyx.org/lyx/lyx-devel/trunk"
SRC_URI="ftp://ftp.lyx.org/pub/lyx/contrib/${FONT_P}.tar.gz
		 linguas_he? (
					   http://cs.haifa.ac.il/~dekelts/lyx/files/hebrew.bind
					   http://cs.haifa.ac.il/~dekelts/lyx/files/preferences
					 )"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="cups debug latex monolithic nls linguas_he"
RESTRICT="test"

RDEPEND="|| (
			  (
				x11-libs/qt-core:4
				x11-libs/qt-gui:4
			  )
			  >=x11-libs/qt-4.2:4
			)
		 x11-libs/libXrandr
		 x11-libs/libXcursor
		 x11-libs/libXrender
		 x11-libs/libXfixes
		 x11-libs/libXext
		 x11-libs/libSM
		 x11-libs/libICE
		 x11-libs/libX11
		 x11-libs/libXau
		 x11-libs/libXdmcp
		 media-gfx/imagemagick
		 media-libs/libpng
		 media-libs/fontconfig
		 media-libs/freetype
		 dev-libs/libxml2
		 dev-libs/boost
		 >=sys-devel/gcc-4.1
		 app-text/aiksaurus
		 app-text/sgmltools-lite
		 virtual/aspell-dict
		 virtual/python
		 cups? ( virtual/lpr )
		 latex? (
				  virtual/latex-base
				  virtual/ghostscript
				  app-text/noweb
				  dev-tex/dvipost
				  dev-tex/chktex
				  || (
					   dev-tex/latex2html
					   dev-tex/tth
					   dev-tex/hevea
					   dev-tex/tex4ht
					 )
				)
		 linguas_he? ( dev-tex/ivritex )"

DEPEND="${RDEPEND}
		x11-proto/xproto
		nls? ( sys-devel/gettext )"

S="${WORKDIR}"/${P}

pkg_setup() {
	if use monolithic; then
		ewarn ":WARNING:"
		ewarn "Bulding LyX with USE=monolithic needs 512Mb of RAM or more"
		ewarn "for each make job (-j). Don't come crying to me if you run"
		ewarn "out of ram."
	fi
}

src_unpack() {
	unpack ${A}
	subversion_fetch || die "Fetch with SVN failed kthnx."
	cd "${S}"
	epatch "${FILESDIR}"/${PN}-1.5.0-gentoo.patch || die

	# trunk needs to regen with autotools
	# lets use gentoo tools instead of the lyx script,
	# which calls aclocal, autoheader, automake, autoconf
	rm -rf "${S}"/autom4te.cache
	eaclocal
	eautoheader
	eautomake
	eautoconf
}

src_compile() {
	append-flags "$(test-flags -fno-stack-protector -fno-stack-protector-all)"
	replace-flags "-Os" "-O2"
	tc-export CXX

	unset LINGUAS

	if use monolithic; then
		monolithic="--enable-monolithic-client"
		monolithic="${monolithic} --enable-monolithic-insets"
		monolithic="${monolithic} --enable-monolithic-mathed"
		monolithic="${monolithic} --enable-monolithic-core"
		monolithic="${monolithic} --enable-monolithic-tex2lyx"
		monolithic="${monolithic} --enable-monolithic-frontend-qt4"
	fi

	if use debug; then
		build_type="--enable-build-type=development"
	else
		build_type="--enable-build-type=release"
	fi

	econf ${monolithic} \
		$(use_enable nls) \
		$(use_enable debug) \
		${build_type} \
		--disable-pch --with-aspell --without-included-boost || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	local mylinguas="${LINGUAS}"

	unset LINGUAS
	emake DESTDIR="${D}" install || die "emake install failed"

	LINGUAS="${mylinguas}"

	dodoc ANNOUNCE ChangeLog NEWS README RELEASE-NOTES UPGRADING

	if use linguas_he ; then
		insinto /usr/share/lyx/bind
		doins "${DISTDIR}"/hebrew.bind
		dodoc "${DISTDIR}"/preferences
	fi

	domenu "${FILESDIR}"/lyx.desktop
	dosed 's:lyx\.xpm:lyx\.png:' /usr/share/appplications/lyx.desktop

	cd "${WORKDIR}"/${FONT_P}
	emake DESTDIR="${D}" install || die "Font installation failed"

	font_src_install

	if use latex ; then
		dosym ../../../lyx/tex /usr/share/texmf/tex/latex/lyx
	fi
}

pkg_postinst() {
	font_pkg_postinst

	if use latex ; then
		texhash
	fi

	if use linguas_he ; then
		elog
		elog "How to use Hebrew in LyX:"
		elog "bunzip2 /usr/share/doc/${PF}/preferences.bz2 into ~/.lyx/preferences"
		elog "or, read http://cs.haifa.ac.il/~dekelts/lyx/instructions2.html"
		elog "for instructions on using lyx's own preferences dialog to equal effect."
		elog
	fi

	elog
	elog "There are known issues in the case of 1.4->1.5 config files transition."
	elog "In case your File->Export list is incomplete try reconfiguring or even"
	elog "remove the old configuration files in ~/.lyx ."
	elog
}

pkg_postrm() {
	if use latex ; then
		texhash
	fi
}
