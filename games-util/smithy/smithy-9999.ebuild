# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit subversion

IUSE=""

DESCRIPTION="A map editor for AlephOne"
HOMEPAGE="http://smithy.sourceforge.net/"
LICENSE=""

DEPEND="dev-lang/ocaml
		dev-ml/lablgl
		dev-ml/lablgtk
		dev-ml/ocaml-make"

SRC_URI=""
ESVN_REPO_URI="https://smithy.svn.sourceforge.net/svnroot/smithy"
SLOT="0"
KEYWORDS=""

pkg_setup() {
	if ! built_with_use dev-ml/lablgtk opengl ; then
		eerror "In order to build ${PN} you need to build lablgtk with"
		eerror "OpenGL support."
		eerror "You need to install dev-ml/lablgtk with opengl useflag on."
		die "Please install lablgtk with opengl useflag"
	fi

	if ! built_with_use --missing true dev-lang/ocaml ocamlopt; then
		eerror "In order to build ${PN}, you first need to have a native"
		eerror "code ocaml compiler."
		eerror "You need to install dev-lang/ocaml with ocamlopt useflag on."
		die "Please install ocaml with ocamlopt useflag"
	fi
}

src_compile() {
	# installation isn't really supported yet, so we have to hack this.
	sed -e 's:resources/:/usr/share/games/smithy/:g' -i Resources.ml

	# if you use emake smithy wont build. have fun figuring out why.
	make
}

src_install () {
	dobin smithy

	insinto '/usr/share/games/smithy'
	doins resources/*

	dodoc docs/* examples/*
}
