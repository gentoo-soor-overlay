# Copyright 2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

# @ECLASS: qt4-qt-copy.eclass
# @MAINTAINER:
# Leonardo Valeri Manera <l.valerimanera@gmail.com>
# @BLURB: Eclass for Qt4 split ebuilds with qt-copy
# @DESCRIPTION:
# This eclass contains overlays the standard qt4-build eclass
# with an optional function that applies qt-copy patches,
# either from a tarball or from svn trunk.

inherit eutils qt4-build

case ${PVR} in
	4.4.0_rc1)
		case ${PN} in
			qt-core|qt-dbus|qt-gui|qt-qt3support)
				QT_COPY_PV="1.2"
				;;
		esac
		;;
	*)
		eerror "Version ${PVR} does not have patchset defined in the qt4-qt-copy eclass!"
		die "Unsupported version for patchset, something screwed up bad, boss."
		;;
esac

if [[ -n ${QT_COPY_PV} ]]; then
		IUSE="${IUSE} +qt-copy"
		SRC_URI="${SRC_URI} qt-copy? ( http://jyujin.de/~creidiki/qt-copy/qt-copy-${PV}-patches-${QT_COPY_PV}.tar.lzma )"
		DEPEND="qt-copy? ( app-arch/lzma-utils )"
fi

qt4_qt-copy() {
	case ${PVR} in
		4.4.0_rc1)
			local patches_corelib_io \
				patches_gui_graphicsview patches_gui_image patches_gui_kernel patches_gui_text patches_gui_widgets \
				patches_qt3support_text
			patches_corelib_io="
				0167-fix-group-reading.diff"
			patches_gui_graphicsview="
				0214-fix-qgraphicsproxywidget-tab-crash.diff"
			patches_gui_image="
				0223-fix-qpixmap-hasalpha.diff
				0224-fast-qpixmap-fill.diff"
			patches_gui_kernel="
				0195-compositing-properties.diff
				0220-no-x-recursion-in-xerrhandler.diff"
			patches_gui_text="
				0203-qtexthtmlparser-link-color.diff"
			patches_gui_widgets="
				0225-invalidate-tabbar-geometry-on-refresh.diff"
			patches_qt3support_text="
				0210-fix-crash-q3stylesheet-font-size.diff"

			local qt_core_patches qt_dbus_patches qt_gui_patches qt_qt3support_patches qt_script_patches
			qt_core_patches="${patches_corelib_io}"
			qt_dbus_patches="${patches_corelib_io}"
			qt_gui_patches="${patches_gui_graphicsview} ${patches_gui_image} ${patches_gui_kernel} ${patches_gui_text} ${patches_gui_widgets}"
			qt_qt3support_patches="${patches_qt3support_text}"
			;;
		*)
			eerror "Version ${PVR} is not supported by the qt4-qt-copy eclass!"
			die "Unsupported version for patching, something screwed up bad, boss."
			;;
	esac

	local qt_copy_patches qt_copy patch qt_copy_files
	qt_copy_patches="${PN//-/_}_patches"
	for qt_copy_patch in ${!qt_copy_patches}; do
		qt_copy_files="${qt_copy_files} patches/${qt_copy_patch}"
	done

	pushd "${S}" &>/dev/null

	echo "lzma -dc ${DISTDIR}/qt-copy-${PV}-patches-${QT_COPY_PV}.tar.lzma | tar xf - --no-same-owner ${qt_copy_files}"
	lzma -dc "${DISTDIR}"/qt-copy-${PV}-patches-${QT_COPY_PV}.tar.lzma | tar xf - --no-same-owner ${qt_copy_files}

	EPATCH_MULTI_MSG="Applying QT-copy patches ..." \
		EPATCH_FORCE="yes" EPATCH_SUFFIX="" epatch "${S}"/patches

	popd &>/dev/null
}

qt4_unpack() {
	local target targets
	for target in configure LICENSE.{GPL2,GPL3,QPL} projects.pro \
		src/{qbase,qt_targets,qt_install}.pri bin config.tests mkspecs qmake \
		${QT4_EXTRACT_DIRECTORIES} ${QT4_TARGET_DIRECTORIES}; do
		targets="${targets} ${MY_P}/${target}"
	done

	echo tar xjpf "${DISTDIR}"/${MY_P}.tar.bz2 ${targets}
	tar xjpf "${DISTDIR}"/${MY_P}.tar.bz2 ${targets}

	case "${PV}" in
		4.4.0_rc*)
			echo tar xjpf "${DISTDIR}"/${MY_P}-headers.tar.bz2
			tar xjpf "${DISTDIR}"/${MY_P}-headers.tar.bz2
			;;
	esac

	use qt-copy && qt4_qt-copy
}

qt4-qt-copy_pkg_postinst() {
	qt4-build_pkg_postinst
	if use qt-copy; then
		ewarn
		ewarn "Do NOT report bugs to Gentoo's bugzilla when built with qt-copy!"
		ewarn
	fi
}

EXPORT_FUNCTIONS pkg_postinst
